'use strict';

// interface Transport {
//
//     static function create() -> Promise(Self)
//
//     function configure(String config) -> Promise()
//
//     function enumerate(Boolean wait) -> Promise([{
//         String path
//         String vendor
//         String product
//         String serialNumber
//         String session
//     }] devices)
//
//     function acquire(String path) -> Promise(String session)
//
//     function release(String session) -> Promise()
//
//     function call(String session, String name, Object data) -> Promise({
//         String name,
//         Object data,
//     })
//
// }

var HttpTransport = require('./transport/http');

// Attempts to load any available HW transport layer
function loadTransport() {
    return HttpTransport.create();
}

module.exports = {
    loadTransport: loadTransport,
    HttpTransport: HttpTransport,
    Session: require('./session'),
    http: require('./http')
};
